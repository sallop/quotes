var app = angular.module('app', []);

app.controller('MainCtrl', function ($scope, $http) {
  $scope.character = {
    "^Shinji Ikari"         :"Eva027.jpg",
    "^Rei Ayanami"          :"Rei.png",
    "^Asuka Langley Sohryuu":"Asuka.jpg",
    "^Misato Katsuragi"     :"Misato.jpg",
    "^Kuzu Fuyutsuki"       :"kozo.jpg",
    "^Ritsuko Akagi"        :"ritsuko.jpg",
    "^Shigeru Aoba"         :"aoba.jpg",
    "^Kaworu Nagisa"        :"Kaworu.jpg",
    "^Gendo Ikari"          :"Gendo.jpg"
  };
  $scope.person = "";  // "^Shinji Ikari";
  $scope.revealed = false;

  $scope.jesus = {"jesus" : "crucified.jpg"};
  // to swap the image 
  $scope.character_of_keys = Object.keys($scope.character);
  $scope.character_length = $scope.character_of_keys.length;

  $scope.count = 0;
  $scope.swap_name = $scope.character_of_keys[$scope.count];
  $scope.swap_image = "crucified.jpg";
  
  console.log("swap_name="+$scope.swap_name);

  $http.get("quotes.json").success(function(data, status, headers, config){
    $scope.data = data;
  }).error(function(data, status, headers, config){
    // called asynchronously if an error occurs
    // or server returns response with an error status.
    console.log("$http.get failed!");
  });

  $scope.increment = function (){
    if ($scope.count < $scope.character_length - 1){
      return $scope.count = $scope.count + 1;
    }
  };
  $scope.decrement = function (){
    if ($scope.count > 0){
      return $scope.count = $scope.count - 1;
    }
  };

  $scope.swap = function(a, b){
    $scope.revealed = true;
    tmp_image = $scope.character[$scope.swap_name];
    $scope.character[$scope.swap_name] = $scope.swap_image;
    $scope.swap_image = tmp_image;
  };

  $scope.reveal = function(){
    var tmp_name, tmp_image;
    if ($scope.revealed == false){
      $scope.revealed = true;
      tmp_image = $scope.character[$scope.swap_name];
      $scope.character[$scope.swap_name] = $scope.swap_image;
      $scope.swap_image = tmp_image;
      console.log($scope.swap_image);
      console.log($scope.character[$scope.swap_name]);
    } else {
      $scope.revealed = false;
      tmp_image = $scope.character[$scope.swap_name];
      $scope.character[$scope.swap_name] = $scope.swap_image;
      $scope.swap_image = tmp_image;
      console.log($scope.swap_image);
      console.log($scope.character[$scope.swap_name]);
    }
  };
})
.directive('quotes', function(){
  return {
    restrict: 'A',
    templateUrl: 'partial.html'
    //template: 'data quote: {{data}}<br>'
  };
})
.filter('sir', function(){
  return function(input, scope){
    for(var name in scope.character){
      var re = new RegExp(name);
      var result = input.search(re);
      //console.log(name +": "+ scope.character[name]);
      if (result != -1){
        scope.person = name;
      }
    }
    return input;
  };
});